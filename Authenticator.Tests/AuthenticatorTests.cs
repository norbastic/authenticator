using Authenticator.BL.Helpers;
using Authenticator.Models;
using Authenticator.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;
using Serilog;
using System;
using Xunit;

namespace Authenticator.Tests
{
    public class AuthenticatorTests
    {
        private readonly ServiceProvider serviceProvider;

        public AuthenticatorTests()
        {
            var services = new ServiceCollection();
            services.AddLogging(logOptions => logOptions.AddSerilog());
            services.AddTransient<IHashGenerator, HashGenerator>();

            serviceProvider = services.BuildServiceProvider();
        }
        [Fact]
        public void EncodeDecodeTest()
        {
            var hashGenerator = serviceProvider.GetService<IHashGenerator>();
            var byteArray = System.Text.Encoding.UTF8.GetBytes("TEST");
            var hash = hashGenerator.GenerateSHA256(byteArray);

            //var uid = AppInstance.GenerateId(hash, "");
            //var decoded = appInstance.DecodeId(uid);

            //Assert.Equal("SZE2329L", decoded.HostName);
            //Assert.Equal("FaragN", decoded.CurrentUser);
        }
    }
}
