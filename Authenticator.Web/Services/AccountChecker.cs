﻿using Authenticator.Web.Data;
using Authenticator.Web.Hubs;
using Authenticator.Web.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using System.DirectoryServices.AccountManagement;

namespace Authenticator.Web.Services
{
    public class AccountChecker : IAccountChecker
    {
        private readonly IHubContext<RunHub> _hubContext;
        private readonly BusinessDbContext _context;

        public AccountChecker(IHubContext<RunHub> hubContext, BusinessDbContext context)
        {
            _hubContext = hubContext;
            _context = context;
        }

        public void EnableAccount(string conid)
        {
            var connection = _context.Connections.Include(x => x.Instance).FirstOrDefault(x => x.ConnectionId == conid);

            var principal = new PrincipalContext(ContextType.Domain, "global.to", "a-1faragn", "");
            var user = UserPrincipal.FindByIdentity(principal, connection.Instance.TargetUser);

            try
            {
                if (user.Enabled == false)
                {
                    user.Enabled = true;
                    user.Save();
                }

                // Let issuer know about the enabled account.
                _hubContext.Clients.Group(ConnectionGroups.Authenticated).SendAsync("AccountEnabled", connection.Instance.TargetUser);
            }
            catch (System.Exception)
            {
                // TODO : Log exception
                _hubContext.Clients.Group(ConnectionGroups.Authenticated).SendAsync("AccountDisabled", connection.Instance.TargetUser);
            }
        }

        public void DisableAccount(string conid)
        {
            // TODO : Disable account
            var connection = _context.Connections?.Include(x => x.Instance).FirstOrDefault(x => x.ConnectionId == conid);

            if (connection == null)
            {
                Task.Run(async () => await _hubContext.Clients.Group(ConnectionGroups.Authenticated).SendAsync("AccountDisabled", connection.Instance.TargetUser));
                return;
            }
            else
            {
                try
                {
                    var principal = new PrincipalContext(ContextType.Domain, "global.to", "a-1faragn", "");
                    var user = UserPrincipal.FindByIdentity(principal, connection?.Instance?.TargetUser);
                    user.Enabled = false;
                    user.Save();

                    Task.Run(async () => await _hubContext.Clients.Group(ConnectionGroups.Authenticated).SendAsync("AccountDisabled", connection.Instance.TargetUser));
                }
                catch (System.Exception)
                {
                }
                finally
                {
                    // Drop connection
                    _context.Connections.Remove(connection);
                    _context.SaveChanges();
                }
            }
        }
    }
}
