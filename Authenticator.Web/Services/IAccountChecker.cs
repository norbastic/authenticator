﻿namespace Authenticator.Web.Services
{
    public interface IAccountChecker
    {
        void EnableAccount(string conid);
        void DisableAccount(string conid);
    }
}