using Authenticator.BL.Models;
using Authenticator.Web.Models;
using Microsoft.EntityFrameworkCore;

namespace Authenticator.Web.Data
{
    public class BusinessDbContext : DbContext
    {
        public BusinessDbContext(DbContextOptions<BusinessDbContext> options)
            : base(options)
        {
        }

        public DbSet<ClientRegistrationRequest> ClientRegistrationRequests { get; set; }
        public DbSet<Client> Clients { get; set; }

        public DbSet<InstanceInfoRequest> InstanceInfoRequests { get; set; }
        public DbSet<RegisteredInstance> RegisteredInstances { get; set; }
        public DbSet<Connection> Connections { get; set; }
    }
}