﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authenticator.Web.Models
{
    public static class ConnectionGroups
    {
        public const string Authenticated = "Authenticated";
        public const string UnAuthenticated = "UnAuthenticated";
    }
}
