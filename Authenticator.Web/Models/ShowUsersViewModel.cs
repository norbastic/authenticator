using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Authenticator.Web.Models
{
    public class ShowUsersViewModel
    {
        public string Email { get; set; }
        public string Role { get; set; }
    }
}