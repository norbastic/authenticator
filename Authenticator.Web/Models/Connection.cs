﻿using Authenticator.BL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Authenticator.Web.Models
{
    public class Connection
    {
        [Key]
        public string ConnectionId { get; set; }
        public InstanceInfo Instance { get; set; }
    }
}
