﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authenticator.Web.Models
{
    public class AddUserToRoleViewModel
    {
        public string User { get; set; }
        public SelectList UserOptions { get; set; }        
        public string Role { get; set; }
        public SelectList RoleOptions { get; set; }
    }

    public class UserViewModel
    {
        public List<IdentityUser> Users { get; set; }
        public List<IdentityRole> Roles { get; set; }
    }

}
