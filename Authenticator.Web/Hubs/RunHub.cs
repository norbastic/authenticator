﻿using Authenticator.BL.Helpers;
using Authenticator.BL.Models;
using Authenticator.Web.Data;
using Authenticator.Web.Models;
using Authenticator.Web.Services;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authenticator.Web.Hubs
{
    public class RunHub : Hub
    {
        private readonly BusinessDbContext _context;
        private readonly IAccountChecker _accountChecker;

        public RunHub(BusinessDbContext dbContext, IAccountChecker connectionChecker)
        {
            _context = dbContext;
            _accountChecker = connectionChecker;
        }
        
        public override async Task OnConnectedAsync()
        {
            
            var context = Context.GetHttpContext();
            var token = context.Request.Query["token"][0];
            if(string.IsNullOrEmpty(token))
            {                
                return;
            }
            else
            {
                var instance = AppInstance.DecodeId(token);

                if (instance == null) return;

                if(IsIntanceRegistered(instance))
                {
                    await Groups.AddToGroupAsync(Context.ConnectionId, ConnectionGroups.Authenticated);
                    await _context.Connections.AddAsync(
                        new Connection() { 
                            ConnectionId = Context.ConnectionId, Instance = instance 
                        });
                    await _context.SaveChangesAsync();
                    _accountChecker.EnableAccount(Context.ConnectionId);
                }
                else
                {
                    _accountChecker.DisableAccount(Context.ConnectionId);
                }
            }
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            try
            {                
                Task.Run(async () => await Groups.RemoveFromGroupAsync(
                    Context.ConnectionId, ConnectionGroups.Authenticated)
                );
            }
            catch (Exception)
            {                
            }
            
            var item = _context.Connections.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            
            if(item != null)
            {
                _context.Connections.Remove(item);
                _context.SaveChanges();
                _accountChecker.DisableAccount(Context.ConnectionId);
            }
            
            return base.OnDisconnectedAsync(exception);
        }

        private bool IsIntanceRegistered(InstanceInfo instance)
        {
            var registeredItem = _context.RegisteredInstances
                        .Where(x => x.Instance.BinaryHash == instance.BinaryHash
                        && x.Instance.HostName == instance.HostName
                        && x.Instance.MAC == instance.MAC
                        && x.Instance.TargetUser == instance.TargetUser)
                        .Include(i => i.Instance)
                        .FirstOrDefault();

            if (registeredItem == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void RunFinished(string user)
        {   
            _accountChecker.DisableAccount(Context.ConnectionId);
        }
    }
}
