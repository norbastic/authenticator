﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Authenticator.Web.Data;
using Authenticator.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace Authenticator.Web.Controllers
{
    public class AdministrationController : Controller
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<IdentityUser> _userManager;
        public AdministrationController(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager)
        {
            _roleManager = roleManager;
            _userManager = userManager;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public IActionResult CreateRole()
        {
            return View();
        }

        [HttpGet]
        public IActionResult AddUserToRole()
        {
            var model = new AddUserToRoleViewModel()
            {
                UserOptions = new SelectList(_userManager.Users?.Select(x => x.UserName).ToList()),
                RoleOptions = new SelectList(_roleManager.Roles?.Select(x => x.Name).ToList())
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddUserToRole(AddUserToRoleViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                if (viewModel.User != null)
                {
                    var user = await _userManager.FindByNameAsync(viewModel.User);
                    if (viewModel.Role != null)
                    {
                        var role = await _roleManager.FindByNameAsync(viewModel.Role);
                        var result = await _userManager.AddToRoleAsync(user, role.Name);
                        if (result.Succeeded)
                        {
                            ViewData["Result"] = "User successfully added to the role.";
                            return View("AddUserToRoleResult");
                        }
                        else
                        {
                            foreach (var error in result.Errors)
                            {
                                ViewData["Result"] = error.Description;
                            }
                        }
                    }

                }                         
            }
            return View("AddUserToRoleResult");
        }

        [HttpPost]
        public async Task<IActionResult> CreateRole(CreateRoleViewModel createRoleViewModel)
        {
            if (ModelState.IsValid)
            {
                var role = new IdentityRole() { Name = createRoleViewModel.RoleName };
                var result = await _roleManager.CreateAsync(role);

                if (result.Succeeded)
                {
                    ViewData["Result"] = "Role has been created successfully.";
                    return View("CreateRoleResult");                    
                }

                foreach(IdentityError error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }

            }

            ViewData["Result"] = "Role could not be created.";
            return View("CreateRoleResult");
        }

        [HttpGet]
        public async Task<IActionResult> ShowUsers()
        {
            var listOfUsers = new List<ShowUsersViewModel>();

            var users = _userManager.Users.ToList();
            var roles = _roleManager.Roles.ToList();
            var userInRole = false;
            var roleName = string.Empty;
            
            foreach (var user in users)
            {
                foreach (var role in roles)
                {
                    userInRole = await _userManager.IsInRoleAsync(user, role.Name);
                    if (userInRole)
                    {
                        roleName = role.Name;
                    }
                }
                listOfUsers.Add(new ShowUsersViewModel(){ Email  = user.Email, Role = roleName});
                roleName = null;
                userInRole = false;
            }

            return View(listOfUsers);
        }
    }
}