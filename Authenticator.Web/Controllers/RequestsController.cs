﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Authenticator.BL.Models;
using Authenticator.Web.Data;
using Microsoft.AspNetCore.Authorization;

namespace Authenticator.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RequestsController : Controller
    {
        private readonly BusinessDbContext _context;

        public RequestsController(BusinessDbContext context)
        {
            _context = context;
        }

        // GET: Requests
        public IActionResult Index()
        {
            var instanceInfo = _context.InstanceInfoRequests
                    .Include(x => x.Instance)
                    .ToList();

            return View(instanceInfo);
        }

        // GET: Requests/Details/5
        public async Task<IActionResult> Approve(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var approvedItem = _context.InstanceInfoRequests
                .Where(m => m.RequestId == id)
                .Include(i => i.Instance)
                .FirstOrDefault();

            if (approvedItem == null)
            {
                return NotFound();
            }
            else
            {
                _context.RegisteredInstances.Add(
                    new RegisteredInstance() { 
                        Instance = approvedItem.Instance, 
                        RegistrationDate = DateTime.Now 
                    });

                _context.InstanceInfoRequests.Remove(approvedItem);
                await _context.SaveChangesAsync();
            }

            return View(approvedItem);
        }

        // GET: Requests/Delete/5
        public async Task<IActionResult> Decline(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var instanceInfoRequest = await _context.InstanceInfoRequests
                .Where(m => m.RequestId == id)
                .Include(i => i.Instance)
                .FirstOrDefaultAsync();

            if (instanceInfoRequest == null)
            {
                return NotFound();
            }

            return View(instanceInfoRequest);
        }

        // POST: Requests/Delete/5
        [HttpPost, ActionName("Decline")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var instanceInfoRequest = await _context.InstanceInfoRequests.FindAsync(id);
            _context.InstanceInfoRequests.Remove(instanceInfoRequest);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool InstanceInfoRequestExists(int id)
        {
            return _context.InstanceInfoRequests.Any(e => e.RequestId == id);
        }
    }
}
