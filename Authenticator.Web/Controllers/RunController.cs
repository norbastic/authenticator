﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Threading.Tasks;
using Authenticator.BL.Models;
using Authenticator.Web.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Authenticator.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RunController : ControllerBase
    {
        private readonly BusinessDbContext _context;
        public RunController(BusinessDbContext dbContext) => _context = dbContext;

        // POST: api/Run
        [HttpPost]
        public void Post([FromBody] InstanceInfo instanceInfo)
        {
            var registeredItem = _context.RegisteredInstances
                .Where(x => x.Instance.BinaryHash == instanceInfo.BinaryHash
                && x.Instance.HostName == instanceInfo.HostName
                && x.Instance.MAC == instanceInfo.MAC
                && x.Instance.TargetUser == instanceInfo.TargetUser)
                .Include(i => i.Instance)
                .FirstOrDefault();

            if (registeredItem == null)
            {
                return;
            }
            else
            {
                var context = new PrincipalContext(ContextType.Domain, "global.to");
                var user = UserPrincipal.FindByIdentity(context, registeredItem.Instance.TargetUser);
                
            }
        }
    }
}
