﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Authenticator.BL.Models;
using Authenticator.Web.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Authenticator.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegistrationController : ControllerBase
    {
        private readonly BusinessDbContext _context;

        public RegistrationController(BusinessDbContext context)
        {
            _context = context;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<InstanceInfoRequest> Register(string clientId, string clientSecret)
        {
            //var request = new InstanceInfoRequest() { DateOfRequest = DateTime.Now, Instance = null };

            var request = new ClientRegistrationRequest()
            {
                RegistrationRequestDate = DateTime.Now,
                ClientInfo = new Client()
                {
                    ClientId = clientId,
                    ClientSecret = clientSecret
                }
            };

            _context.Database.EnsureCreated();
            _context.ClientRegistrationRequests.Add(request);
            _context.SaveChanges();

            return CreatedAtAction(nameof(Register), new {id = request.Id}, request);
        }
    }
}