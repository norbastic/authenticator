﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Authenticator.BL.Models;
using Authenticator.Web.Data;
using Microsoft.AspNetCore.Authorization;

namespace Authenticator.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RegisteredInstancesController : Controller
    {
        private readonly BusinessDbContext _context;

        public RegisteredInstancesController(BusinessDbContext context)
        {
            _context = context;
        }

        // GET: RegisteredInstances
        public async Task<IActionResult> Index()
        {
            var registeredInstances = _context.RegisteredInstances
                .Include(i => i.Instance)
                .ToListAsync();

            return View(await registeredInstances);
        }

        // GET: RegisteredInstances/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var registeredInstance = await _context.RegisteredInstances
                .Where(m => m.Id == id)
                .Include(i => i.Instance)
                .FirstOrDefaultAsync();

            if (registeredInstance == null)
            {
                return NotFound();
            }

            return View(registeredInstance);
        }

        // POST: RegisteredInstances/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var registeredInstance = await _context.RegisteredInstances.FindAsync(id);
            _context.RegisteredInstances.Remove(registeredInstance);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RegisteredInstanceExists(int id)
        {
            return _context.RegisteredInstances.Any(e => e.Id == id);
        }
    }
}
