﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Authenticator.Web.Migrations
{
    public partial class Clients2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "RegistrationRequestDate",
                table: "Clients");

            migrationBuilder.CreateTable(
                name: "ClientRegistrationRequests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RegistrationRequestDate = table.Column<DateTime>(nullable: false),
                    ClientInfoClientId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientRegistrationRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientRegistrationRequests_Clients_ClientInfoClientId",
                        column: x => x.ClientInfoClientId,
                        principalTable: "Clients",
                        principalColumn: "ClientId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ClientRegistrationRequests_ClientInfoClientId",
                table: "ClientRegistrationRequests",
                column: "ClientInfoClientId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClientRegistrationRequests");

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Clients",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTime>(
                name: "RegistrationRequestDate",
                table: "Clients",
                type: "datetime2",
                nullable: true);
        }
    }
}
