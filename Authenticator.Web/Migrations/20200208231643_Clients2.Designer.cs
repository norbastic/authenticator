﻿// <auto-generated />
using System;
using Authenticator.Web.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Authenticator.Web.Migrations
{
    [DbContext(typeof(BusinessDbContext))]
    [Migration("20200208231643_Clients2")]
    partial class Clients2
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.0")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Authenticator.BL.Models.Client", b =>
                {
                    b.Property<string>("ClientId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ClientSecret")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("ClientId");

                    b.ToTable("Clients");
                });

            modelBuilder.Entity("Authenticator.BL.Models.ClientRegistrationRequest", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClientInfoClientId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("RegistrationRequestDate")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("ClientInfoClientId");

                    b.ToTable("ClientRegistrationRequests");
                });

            modelBuilder.Entity("Authenticator.BL.Models.InstanceInfo", b =>
                {
                    b.Property<int>("InstanceId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("BinaryHash")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("CurrentUser")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("HostName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("MAC")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("TargetUser")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("InstanceId");

                    b.ToTable("InstanceInfo");
                });

            modelBuilder.Entity("Authenticator.BL.Models.InstanceInfoRequest", b =>
                {
                    b.Property<int>("RequestId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("DateOfRequest")
                        .HasColumnType("datetime2");

                    b.Property<int?>("InstanceId")
                        .HasColumnType("int");

                    b.HasKey("RequestId");

                    b.HasIndex("InstanceId");

                    b.ToTable("InstanceInfoRequests");
                });

            modelBuilder.Entity("Authenticator.BL.Models.RegisteredInstance", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("InstanceId")
                        .HasColumnType("int");

                    b.Property<DateTime>("RegistrationDate")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("InstanceId");

                    b.ToTable("RegisteredInstances");
                });

            modelBuilder.Entity("Authenticator.Web.Models.Connection", b =>
                {
                    b.Property<string>("ConnectionId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int?>("InstanceId")
                        .HasColumnType("int");

                    b.HasKey("ConnectionId");

                    b.HasIndex("InstanceId");

                    b.ToTable("Connections");
                });

            modelBuilder.Entity("Authenticator.BL.Models.ClientRegistrationRequest", b =>
                {
                    b.HasOne("Authenticator.BL.Models.Client", "ClientInfo")
                        .WithMany()
                        .HasForeignKey("ClientInfoClientId");
                });

            modelBuilder.Entity("Authenticator.BL.Models.InstanceInfoRequest", b =>
                {
                    b.HasOne("Authenticator.BL.Models.InstanceInfo", "Instance")
                        .WithMany()
                        .HasForeignKey("InstanceId");
                });

            modelBuilder.Entity("Authenticator.BL.Models.RegisteredInstance", b =>
                {
                    b.HasOne("Authenticator.BL.Models.InstanceInfo", "Instance")
                        .WithMany()
                        .HasForeignKey("InstanceId");
                });

            modelBuilder.Entity("Authenticator.Web.Models.Connection", b =>
                {
                    b.HasOne("Authenticator.BL.Models.InstanceInfo", "Instance")
                        .WithMany()
                        .HasForeignKey("InstanceId");
                });
#pragma warning restore 612, 618
        }
    }
}
