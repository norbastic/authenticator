﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Authenticator.Web.Migrations
{
    public partial class Clients3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClientRegistrationRequests_Clients_ClientInfoClientId",
                table: "ClientRegistrationRequests");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Clients",
                table: "Clients");

            migrationBuilder.DropIndex(
                name: "IX_ClientRegistrationRequests_ClientInfoClientId",
                table: "ClientRegistrationRequests");

            migrationBuilder.DropColumn(
                name: "ClientInfoClientId",
                table: "ClientRegistrationRequests");

            migrationBuilder.AlterColumn<string>(
                name: "ClientId",
                table: "Clients",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "Clients",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<int>(
                name: "ClientInfoId",
                table: "ClientRegistrationRequests",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Clients",
                table: "Clients",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_ClientRegistrationRequests_ClientInfoId",
                table: "ClientRegistrationRequests",
                column: "ClientInfoId");

            migrationBuilder.AddForeignKey(
                name: "FK_ClientRegistrationRequests_Clients_ClientInfoId",
                table: "ClientRegistrationRequests",
                column: "ClientInfoId",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClientRegistrationRequests_Clients_ClientInfoId",
                table: "ClientRegistrationRequests");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Clients",
                table: "Clients");

            migrationBuilder.DropIndex(
                name: "IX_ClientRegistrationRequests_ClientInfoId",
                table: "ClientRegistrationRequests");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "ClientInfoId",
                table: "ClientRegistrationRequests");

            migrationBuilder.AlterColumn<string>(
                name: "ClientId",
                table: "Clients",
                type: "nvarchar(450)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClientInfoClientId",
                table: "ClientRegistrationRequests",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Clients",
                table: "Clients",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientRegistrationRequests_ClientInfoClientId",
                table: "ClientRegistrationRequests",
                column: "ClientInfoClientId");

            migrationBuilder.AddForeignKey(
                name: "FK_ClientRegistrationRequests_Clients_ClientInfoClientId",
                table: "ClientRegistrationRequests",
                column: "ClientInfoClientId",
                principalTable: "Clients",
                principalColumn: "ClientId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
