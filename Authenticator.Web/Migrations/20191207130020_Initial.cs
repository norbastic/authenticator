﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Authenticator.Web.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InstanceInfo",
                columns: table => new
                {
                    InstanceId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    HostName = table.Column<string>(nullable: true),
                    MAC = table.Column<string>(nullable: true),
                    CurrentUser = table.Column<string>(nullable: true),
                    BinaryHash = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstanceInfo", x => x.InstanceId);
                });

            migrationBuilder.CreateTable(
                name: "InstanceInfoRequests",
                columns: table => new
                {
                    RequestId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateOfRequest = table.Column<DateTime>(nullable: false),
                    InstanceId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstanceInfoRequests", x => x.RequestId);
                    table.ForeignKey(
                        name: "FK_InstanceInfoRequests_InstanceInfo_InstanceId",
                        column: x => x.InstanceId,
                        principalTable: "InstanceInfo",
                        principalColumn: "InstanceId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InstanceInfoRequests_InstanceId",
                table: "InstanceInfoRequests",
                column: "InstanceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InstanceInfoRequests");

            migrationBuilder.DropTable(
                name: "InstanceInfo");
        }
    }
}
