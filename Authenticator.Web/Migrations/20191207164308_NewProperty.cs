﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Authenticator.Web.Migrations
{
    public partial class NewProperty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "TargetUser",
                table: "InstanceInfo",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TargetUser",
                table: "InstanceInfo");
        }
    }
}
