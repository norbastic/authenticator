﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Authenticator.Web.Migrations
{
    public partial class RegisteredInstances : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RegisteredInstances",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RegistrationDate = table.Column<DateTime>(nullable: false),
                    InstanceId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RegisteredInstances", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RegisteredInstances_InstanceInfo_InstanceId",
                        column: x => x.InstanceId,
                        principalTable: "InstanceInfo",
                        principalColumn: "InstanceId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RegisteredInstances_InstanceId",
                table: "RegisteredInstances",
                column: "InstanceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RegisteredInstances");
        }
    }
}
