﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Text;

namespace Authenticator.Helpers
{
    public class Console
    {
        private static SecureString GetSecureString()
        {
            SecureString pass = new SecureString();
            System.Console.Write($"Enter your password: ");
            do
            {
                ConsoleKeyInfo key = System.Console.ReadKey(true);
                // Backspace Should Not Work
                if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
                {
                    pass.AppendChar(key.KeyChar);
                    System.Console.Write("*");
                }
                else
                {
                    if (key.Key == ConsoleKey.Enter)
                    {
                        break;
                    }
                }
            } while (true);

            return pass;
        }
    }
}
