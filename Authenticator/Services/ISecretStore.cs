﻿namespace Authenticator.Services
{
    public interface ISecretStore
    {
        (string, string) Retrieve();
        bool Store(string clientId, string clientSecret);
    }
}