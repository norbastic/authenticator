﻿namespace Authenticator.Services
{
    public interface IHashGenerator
    {
        string GenerateSHA256(byte[] binaryPath);
    }
}