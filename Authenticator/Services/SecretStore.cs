﻿using Authenticator.Models;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.IO;

namespace Authenticator.Services
{
	public class SecretStore : ISecretStore
	{
		private readonly IDataProtector _protector;
		private readonly ILogger<SecretStore> _logger;
		private const string path = @"./clientdata.json";

		public SecretStore(ILogger<SecretStore> logger, IDataProtectionProvider dataProtectionProvider)
		{
			_logger = logger;
			_protector = dataProtectionProvider.CreateProtector("Authenticator.Services.SecretStore");
		}

		public bool Store(string clientId, string clientSecret)
		{
			if (File.Exists(path))
			{
				return false;
			}

			try
			{
				var protectedId = _protector.Protect(clientId);
				var protectedSecret = _protector.Protect(clientSecret);

				var client = new ClientData()
				{
					ClientId = protectedId,
					ClientSecret = protectedSecret
				};

				File.WriteAllText(path, JsonConvert.SerializeObject(client));

				return true;
			}

			catch (System.Exception ex)
			{
				_logger.LogError(ex.Message);
				return false;
			}
		}

		public (string, string) Retrieve()
		{
			if (!File.Exists(path))
			{
				return (null, null);
			}

			string json = string.Empty;

			try
			{
				json = File.ReadAllText(path);
			}
			catch (System.Exception ex)
			{
				_logger.LogError(ex.Message);				
			}

			var obj = JsonConvert.DeserializeObject<ClientData>(json);
			
			var unprotectedId = _protector.Unprotect(obj?.ClientId);
			var unprotectedSecret = _protector.Unprotect(obj?.ClientSecret);

			return (unprotectedId, unprotectedSecret);
		}
	}
}

