﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Authenticator.Services
{
    public static class RandomGenerator
    {
        public static string GetRandom()
        {
            var rngCrypto = new RNGCryptoServiceProvider();
            var randomNumber = new byte[128];
            rngCrypto.GetBytes(randomNumber);
            var randomString = Convert.ToBase64String(randomNumber);

            return randomString;
        }
    }
}
