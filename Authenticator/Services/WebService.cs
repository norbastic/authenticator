﻿using Authenticator.BL.Helpers;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using Microsoft.Extensions.Configuration;
using Authenticator.Models;

namespace Authenticator.Services
{
    public class WebService : IWebService
    {
        private string _url = "https://localhost:44375/api/registration";
        private readonly IConfiguration _configuration;
        public WebService(IConfiguration configuration)
        {
            _configuration = configuration;
            _url = _configuration.GetValue<string>("registrationServer");
        }

        public async void RegisterInstance(string clientId, string clientSecret)
        {

            if (clientId == null || clientSecret == null) return;

            using (var client = new HttpClient())
            {
                var clientData = new ClientData() { ClientId = clientId, ClientSecret = clientSecret };
                var json = JsonConvert.SerializeObject(clientData);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var result = await client.PostAsync(_url, content);
            }

        }

    }
}
