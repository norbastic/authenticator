﻿using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Authenticator.Services
{
    public class HashGenerator : IHashGenerator
    {
        private ILogger<HashGenerator> _logger;

        public HashGenerator(ILogger<HashGenerator> logger)
        {
            _logger = logger; 
        }

        public string GenerateSHA256(byte[] binaryPath)
        {
            try
            {                                
                using var sha256Hash = SHA256.Create();
                var result = sha256Hash.ComputeHash(binaryPath);
                
                var stringBuilder = new StringBuilder();
                foreach (var chunk in result)
                {
                    var hex = chunk.ToString("x2");
                    stringBuilder.Append(hex);
                }

                return stringBuilder.ToString();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occured during hash generation. Exception: {ex.Message}");
                return string.Empty;
            }
        }
    }
}
