﻿namespace Authenticator.Services
{
    public interface IWebService
    {
        void RegisterInstance(string clientId, string clientSecret);
    }
}