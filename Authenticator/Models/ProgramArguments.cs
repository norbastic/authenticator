﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Text;

namespace Authenticator.Models
{
    public class ProgramArguments
    {
        [Option('r', "registerid", Required = false, HelpText = "Add this option if you would like to register this instance.")]
        public bool IsRegister { get; set; }
        [Option('b', "binary", Required = true, HelpText = "This is the path to the binary which needs to be registered or launched.")]
        public string Binary { get; set; }
        [Option('u', "username", Required = true, HelpText = "The username which is required to the program run.")]
        public string Username { get; set; }
    }
}
