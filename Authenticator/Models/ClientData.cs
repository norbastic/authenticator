﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Authenticator.Models
{
    public class ClientData
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}
