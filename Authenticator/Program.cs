﻿using Authenticator.Models;
using CommandLine;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using Serilog;
using Authenticator.Services;
using Microsoft.AspNetCore.SignalR.Client;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.Win32;
using Authenticator.BL.Helpers;
using Microsoft.Extensions.Configuration;

namespace Authenticator
{
    class Program
    {
        private static ILogger<Program> logger;
        private static HubConnection connection;
        private static ManualResetEvent manualResetEvent = new ManualResetEvent(false);
        private static string userName = string.Empty;
        static void Main(string[] args)
        {           
            var services = CreateDI();            
            logger = services.GetService<ILogger<Program>>();
            var hashGenerator = services.GetService<IHashGenerator>();
            var webClient = services.GetService<IWebService>();
            var secretStore = services.GetService<ISecretStore>();            

            logger.LogInformation("Starting application.");
            Console.ReadKey();

            Parser.Default.ParseArguments<ProgramArguments>(args).WithParsed(a =>
            {
                var binaryHash = string.Empty;

                if (!File.Exists(a.Binary))
                {
                    logger.LogError("Exiting application... The specified binary does not exists.");
                    Environment.Exit(1);
                }
                else
                {
                    binaryHash = hashGenerator.GenerateSHA256(File.ReadAllBytes(a.Binary));
                }

                if ((a.IsRegister) && (a.Username != null))
                {
                    logger.LogInformation("Registering application instance");
                    var programUid = AppInstance.GenerateId(
                        binaryHash, 
                        a.Username, 
                        EnvironmentInfo.GetDnsHostName(), 
                        EnvironmentInfo.GetFirstMAC(), 
                        EnvironmentInfo.GetCurrentUser());

                    var secret = RandomGenerator.GetRandom();
                    if(secretStore.Store(programUid, secret))
                    {
                        webClient.RegisterInstance(programUid, secret);
                    }

                    Console.WriteLine("Press any key to exit.");
                    Console.ReadKey();
                }

                if ((a.IsRegister == false) && (a.Username != null))
                {
                    logger.LogInformation($"Request launch with username: {a.Username}");
                    var result = secretStore.Retrieve();

                    connection = Task.Run(async () => await ConnectToHub(result.Item1)).Result;
                    
                    connection.On<string>("AccountEnabled", (u) => OnAccountEnabled(u, a.Binary));

                    connection.On<string>("AccountDisabled", (u) => OnAccountDisabled(u));
                    
                    manualResetEvent.WaitOne();
                }
            });            
        }

        private static void OnAccountEnabled(string user, string path)
        {
            userName = user;
            logger.LogDebug($"Account is enabled. {user}");

            var processInfo = new ProcessStartInfo(path);
            processInfo.UseShellExecute = true;

            var process = Process.Start(processInfo);
            logger.LogInformation("Process is started");
            process.EnableRaisingEvents = true;
            process.Exited += Process_Exited;            
        }

        private static void Process_Exited(object sender, EventArgs e)
        {
            logger.LogInformation("Exiting application.");
            connection.SendAsync("RunFinished", userName);            
        }
        
        private static void OnAccountDisabled(string user)
        {
            userName = string.Empty;
            logger.LogDebug($"Account is disabled. {user}");
            manualResetEvent.Set();
        }

        static async Task<HubConnection> ConnectToHub(string runId)
        {
            var connection = new HubConnectionBuilder()
            .WithUrl($"https://localhost:44375/running?token={runId}")            
            .Build();

            await connection.StartAsync();

            return connection;
        }

        static IServiceProvider CreateDI()
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.File("logs/log-.txt", rollingInterval: RollingInterval.Day)
                .WriteTo.Console()
                .CreateLogger();

            var config = BuildConfiguration();

            var services = new ServiceCollection();
            services.AddLogging(logOptions => logOptions.AddSerilog());
            services.AddTransient<IHashGenerator, HashGenerator>();
            services.AddTransient<IWebService, WebService>();
            services.AddTransient<ISecretStore, SecretStore>();
            services.AddSingleton(config);
            Registry.CurrentUser.CreateSubKey(@"SOFTWARE\HYDRO\Authenticator\keys", RegistryKeyPermissionCheck.ReadWriteSubTree);

            services.AddDataProtection()
                .PersistKeysToRegistry(Registry.CurrentUser.OpenSubKey(@"SOFTWARE\HYDRO\Authenticator\keys", true))
                .SetDefaultKeyLifetime(TimeSpan.FromDays(365))
                .ProtectKeysWithDpapi();
            
            var serviceProvider = services.BuildServiceProvider();

            return serviceProvider;            
        }

        static IConfiguration BuildConfiguration()
        {
            var configBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            return configBuilder.Build();
        }
    }
}
