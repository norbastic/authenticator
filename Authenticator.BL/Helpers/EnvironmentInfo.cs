﻿using Serilog;
using System;
using System.Linq;

namespace Authenticator.BL.Helpers
{
    public static class EnvironmentInfo
    {
        public static string GetFirstMAC()
        {
            try
            {
                var nics = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces();
                var mac = nics.FirstOrDefault().GetPhysicalAddress().ToString();
                Log.Verbose($"MAC of the first NIC {mac}");
                return mac;
            }
            catch (Exception ex)
            {
                Log.Error($"Unable to get MAC address of the first NIC. Exception: {ex.Message}");
                return string.Empty;
            }
        }

        public static string GetDnsHostName()
        {
            try
            {
                var hostName = System.Net.Dns.GetHostName();
                Log.Verbose($"Hostname of the computer: {hostName}");
                return hostName;
            }
            catch (Exception ex)
            {
                Log.Error($"Could not get computer name. Exception: {ex.Message}");
                return string.Empty;
            }
        }

        public static string GetCurrentUser()
        {
            try
            {
                var user = Environment.UserName;
                Log.Verbose($"Current username is {user}");
                return user;
            }
            catch (Exception ex)
            {
               Log.Error($"Could not get currently logged in user. Exception: {ex.Message}");
                return string.Empty;
            }
        }
    }
}
