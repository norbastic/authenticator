﻿using Authenticator.BL.Models;
using System;
using System.Text;

namespace Authenticator.BL.Helpers
{
    public static class AppInstance
    {
        /// <summary>
        /// Decodes the ClientId into InstanceInfo object
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static InstanceInfo DecodeId(string id)
        {
            var baseBytes = Convert.FromBase64String(id);
            var decodedStr = Encoding.UTF8.GetString(baseBytes);

            var resultSet = decodedStr.Split(":");
            var result = new InstanceInfo() { 
                HostName = resultSet[0], 
                MAC = resultSet[1], 
                CurrentUser = resultSet[2],
                TargetUser = resultSet[3],
                BinaryHash = resultSet[4] 
            };

            return result;
        }
        /// <summary>
        /// Generates ClientID encoded in BASE64
        /// </summary>
        /// <param name="binaryHash"></param>
        /// <param name="targetUser"></param>
        /// <param name="dnsHostName"></param>
        /// <param name="MAC"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public static string GenerateId(string binaryHash, string targetUser, string dnsHostName, string MAC, string currentUser)
        {
            var toConvert = $"{dnsHostName}:{MAC}:{currentUser}:{targetUser}:{binaryHash}";
            var toConvertBytes = Encoding.UTF8.GetBytes(toConvert);

            return Convert.ToBase64String(toConvertBytes);
        }
    }
}
