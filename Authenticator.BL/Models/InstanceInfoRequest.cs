﻿using Authenticator.BL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Authenticator.BL.Models
{
    public class InstanceInfoRequest
    {
        [Key]
        public int RequestId { get; set; }
        public DateTime DateOfRequest { get; set; }
        public InstanceInfo Instance { get; set; }
    }
}
