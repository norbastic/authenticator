using System;
using System.ComponentModel.DataAnnotations;

namespace Authenticator.BL.Models
{
	public class ClientRegistrationRequest
    {
        [Key]
        public int Id { get; set; }
        public DateTime RegistrationRequestDate { get; set; }
        public Client ClientInfo { get; set; }
    }
}