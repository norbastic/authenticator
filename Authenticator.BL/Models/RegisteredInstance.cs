﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Authenticator.BL.Models
{
    public class RegisteredInstance
    {
        [Key]
        public int Id { get; set; }
        public DateTime RegistrationDate { get; set; }
        public InstanceInfo Instance { get; set; }
    }
}
