﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Authenticator.BL.Models
{
    public class InstanceInfo
    {
        [Key]
        public int InstanceId { get; set; }
        public string HostName { get; set; }
        public string MAC { get; set; }
        public string CurrentUser { get; set; }
        public string BinaryHash { get; set; }
        public string TargetUser { get; set; }
    }
}
